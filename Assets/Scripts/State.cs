﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "State_", menuName = "NPC/State")]
public class State : ScriptableObject
{
    public Action[] actions = new Action[1];
    public Transition[] transitions = new Transition[1];
    public Color sceneGizmoColor = Color.grey;

    public void UpdateState(Actor actor)
    {
        DoActions(actor);
        CheckTransitions(actor);
    }

    public void DoActions(Actor actor)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i]?.Act(actor);
        }
    }

    public void CheckTransitions(Actor actor)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(actor);

            if (decisionSucceeded)
            {
                actor.TransitionToState(transitions[i].trueState[Random.Range(0, transitions[i].trueState.Length)]);
            }
            else
            {
                actor.TransitionToState(transitions[i].falseState[Random.Range(0, transitions[i].falseState.Length)]);
            }
        }
    }
}
