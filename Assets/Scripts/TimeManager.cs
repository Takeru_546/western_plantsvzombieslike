﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    [Range(0,5)]public float timeScale = 1;

    void Update()
    {
        Time.timeScale = timeScale;
    }
}
