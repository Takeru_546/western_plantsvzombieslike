﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatform : MonoBehaviour
{
    PlatformSlot myPlatform;

    private void Start()
    {
        myPlatform = GetComponent<PlatformSlot>();
    }

    void Update()
    {
        if(myPlatform.currentActor)
        {
            if (myPlatform.currentActor.npcType == Actor.Type.ENEMY)
            {
                Player.instance.currentPlayerHealth--;
                Destroy(myPlatform.currentActor.gameObject);
                myPlatform.currentActor = null;
            }
        }

    }
}
