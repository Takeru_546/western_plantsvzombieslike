﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    PlatformSlot myPlatform;
    public Actor[] enemies;
    public float minTimer = 10;
    public float maxTimer = 20;
    

    private float nextActionTime = 10;

    void Start()
    {
        myPlatform = GetComponent<PlatformSlot>();
        nextActionTime = Random.Range(minTimer, maxTimer);
    }


    void Update()
    {
        if(CheckTimerContinuous(Random.Range(minTimer, maxTimer)) && myPlatform.currentActor == null)
        {
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        myPlatform.currentActor = Instantiate(enemies[Random.Range(0, enemies.Length)], myPlatform.transform.position, Quaternion.identity);
        myPlatform.currentActor.isActive = true;
        myPlatform.currentActor.myPlatform = myPlatform;
    }

    public bool CheckTimerContinuous(float timeToAdd)
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime = Time.time + timeToAdd;
            return true;
        }
        else return false;
    }
}
