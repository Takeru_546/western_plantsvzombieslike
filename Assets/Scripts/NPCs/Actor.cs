﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


public class Actor : MonoBehaviour
{
    public enum Type { ENEMY, TROOP };
    public Type npcType;
    public PlatformSlot myPlatform;

    public bool paid = false;

    [HideInInspector] public ProjectileShooter projectileShooter;
    public Transform shootPos;

    [HideInInspector] public NavMeshAgent navMeshAgent;


    [Header("STATE MACHINE")]

    public bool isActive = false;
    public State currentState;
    public State remainState;
    public State startingState;

    [Header("STATS")]
      
    public int baseHealth = 5;
    [HideInInspector]public int currentHealth;
    public int money = 1;

    [Header("UI")]
    public Slider healthBar;

    void Start()
    {
        currentState = startingState;
        currentHealth = baseHealth;
        healthBar.maxValue = baseHealth;

        projectileShooter = GetComponent<ProjectileShooter>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void Update()
    {
        if (isActive)
            currentState.UpdateState(this);

        //Checks if actor is dead
        if(currentHealth <= 0)
        {
            Die();
        }

        //update UI
        healthBar.value = currentHealth;

    }

    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            ChangeState(nextState);

            currentState = nextState;
        }
    }

    public void ChangeState(State stateToChange)
    {
        currentState = stateToChange;
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
    }

    public void Die()
    {
        myPlatform.currentActor = null;
        Player.instance.money += money;
        Destroy(gameObject);
    }
}
