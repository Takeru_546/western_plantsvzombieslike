﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "NPC/Decisions/CheckDistance", fileName = "Decision_CheckDistance")]
public class DecisionCheckDistance : Decision
{
    public Actor.Type targetType;

    public bool facingRight = true;
    public int distance;

    public override bool Decide(Actor actor)
    {
        if(distance > 0)
        {
            if(facingRight)
            {
                for (int i = 1; i <= distance; i++)
                {
                    var currentJ = actor.myPlatform.matrixJ + i;
                    //Check if range does not exceed maximum length
                    if (currentJ >= Player.instance.platformSlots.GetLength(1) || currentJ < 0)
                        return false;

                    Debug.Log("Checking matrix i: " + actor.myPlatform.matrixI + ", j: " + currentJ);
                    //Check if there is an actor
                    if (CheckEnemy(actor, currentJ))
                    {
                        if (actor.navMeshAgent)
                        {
                            actor.navMeshAgent.isStopped = true;
                            actor.navMeshAgent.ResetPath();
                        }
                        return true;
                    }
                }

                return false;
            }
            else
            {
                for (int i = 1; i <= distance; i++)
                {
                    var currentJ = actor.myPlatform.matrixJ - i;
                    //Check if range does not exceed maximum length
                    if (currentJ >= Player.instance.platformSlots.GetLength(1) || currentJ < 0)
                        return false;

                    //Debug.Log("Checking matrix i: " + actor.myPlatform.matrixI + ", j: " + currentJ);
                    //Check if there is an actor
                    if (CheckEnemy(actor, currentJ))
                    {
                        if(actor.navMeshAgent)
                        {
                            actor.navMeshAgent.isStopped = true;
                            actor.navMeshAgent.ResetPath();
                        }
                        return true;
                    }
                }

                return false;
            }

        }

        return false;
    }

    public bool CheckEnemy(Actor actor, int jValue)
    {
        //Check if there is an actor
        if (Player.instance.platformSlots[actor.myPlatform.matrixI, jValue].currentActor)
        {
            //Check if there is an actor in range
            if (Player.instance.platformSlots[actor.myPlatform.matrixI, jValue].currentActor.npcType == targetType)
            {
                Debug.Log("ENEMY FOUND: " + Player.instance.platformSlots[actor.myPlatform.matrixI, jValue].currentActor.name);
                return true;
            }
        }
        return false;
    }
}
