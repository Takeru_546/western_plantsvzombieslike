﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "NPC/Action/GenerateMoney", fileName = "Action_GenerateMoney")]
public class ActionGenereateMoney : Action
{

    public int moneyGenerated = 1;
    public int moneyRate;

    public override void Act(Actor actor)
    {
        if(actor.projectileShooter.CheckTimerContinuous(moneyRate))
        {
            Player.instance.money += moneyRate;
        }
    }
}
