﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "NPC/Action/Idle")]
public class ActionIdle : Action
{
    public override void Act(Actor actor)
    {
        Debug.Log("Idle");
    }
}
