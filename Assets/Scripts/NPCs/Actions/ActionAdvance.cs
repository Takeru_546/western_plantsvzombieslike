﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "NPC/Action/Advance", fileName = "Action_Advance")]
public class ActionAdvance : Action
{    
    public override void Act(Actor actor)
    {       
        if(actor.navMeshAgent != null)
        {
            if (actor.myPlatform.matrixJ - 1 >= 0)
            {
                if (Player.instance.platformSlots[actor.myPlatform.matrixI, actor.myPlatform.matrixJ - 1].currentActor == actor ||
                    !Player.instance.platformSlots[actor.myPlatform.matrixI, actor.myPlatform.matrixJ - 1].currentActor)
                {
                    actor.navMeshAgent.isStopped = false;
                    actor.navMeshAgent.SetDestination(Player.instance.platformSlots[actor.myPlatform.matrixI, actor.myPlatform.matrixJ - 1].transform.position);
                    
                    if (actor.navMeshAgent.remainingDistance < .075f)
                    {
                        if (Player.instance.platformSlots[actor.myPlatform.matrixI, actor.myPlatform.matrixJ - 1].currentActor == actor ||
                            !Player.instance.platformSlots[actor.myPlatform.matrixI, actor.myPlatform.matrixJ - 1].currentActor)
                        {
                            actor.myPlatform.currentActor = null;
                            actor.myPlatform = Player.instance.platformSlots[actor.myPlatform.matrixI, actor.myPlatform.matrixJ - 1];
                            actor.myPlatform.currentActor = actor;

                            actor.navMeshAgent.isStopped = true;
                            actor.navMeshAgent.ResetPath();
                        }

                    }
                }
            }


        
        }
    }
}
