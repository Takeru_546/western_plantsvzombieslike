﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "NPC/Action/Shoot", fileName = "Action_Shoot")]
public class ActionShoot : Action
{
    public int damage = 1;
    public float fireRate = 1;

    public override void Act(Actor actor)
    {
        if(actor.projectileShooter.CheckTimerContinuous(fireRate))
        {
            actor.projectileShooter.Shoot(damage, actor.shootPos);
        }
    }
}
