﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{
    public Projectile projectile;

    private float nextActionTime = .1f;


    public void Shoot(int damage, Transform shootPos)
    {
        Instantiate(projectile, shootPos.position, shootPos.rotation);
        projectile.damage = damage;
    }

    public bool CheckTimerContinuous(float timeToAdd)
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime = Time.time + timeToAdd;
            return true;
        }
        else return false;
    }
}
