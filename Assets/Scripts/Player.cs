﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player instance;

    public LayerMask platformLayermask;
    public RaycastHit mouseRayHit;

    [SerializeField]private PlatformSlot[] basePlatforms;
    public PlatformSlot[,] platformSlots = new PlatformSlot[5,7];

    public PlatformSlot selectedPlatform;
    public Actor objectSelected;

    [Header("STATS")]
    public int money = 50;
    public int basePlayerHealth = 25;
    [HideInInspector] public int currentPlayerHealth;
    

    [Header("UI")]
    
    public TextMeshProUGUI moneyText;
    public Slider healthBar;
    public GameObject deathPanel;






    private void Awake()
    {
        instance = this;
        int currentPlatform = 0;

        for (int j = 0; j < platformSlots.GetLength(1); j++)
        {
            for (int i = 0; i < platformSlots.GetLength(0); i++)
            {
                platformSlots[i, j] = basePlatforms[currentPlatform];
                platformSlots[i, j].matrixI = i;
                platformSlots[i, j].matrixJ = j;
                currentPlatform++;
            }
        }

        currentPlayerHealth = basePlayerHealth;
        healthBar.maxValue = basePlayerHealth;
    }


    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        bool rayHasHit = Physics.Raycast(ray, out mouseRayHit, Mathf.Infinity, platformLayermask);
        // Debug.Log(rayHasHit);
        if (rayHasHit)
        {
            selectedPlatform = mouseRayHit.collider.GetComponentInParent<PlatformSlot>();
        }

        CheckObjectReplace();

        UpdateUI();
    }

    private void CheckObjectReplace()
    {
        if (selectedPlatform && selectedPlatform.currentActor && !objectSelected)
        {
            if (Input.GetMouseButtonDown(0) && selectedPlatform.currentActor.npcType == Actor.Type.TROOP)
            {
                objectSelected = selectedPlatform.currentActor;
                selectedPlatform.currentActor = null;
                objectSelected.isActive = false;
            }
        }

        if (objectSelected && selectedPlatform)
        {
            objectSelected.transform.position = selectedPlatform.transform.position;
            if (Input.GetMouseButtonUp(0))
            {
                if (!selectedPlatform.GetComponent<EnemySpawner>())
                    PlaceObject();
                else
                    Destroy(objectSelected.gameObject);
            }
        }
    }

    private void UpdateUI()
    {
        moneyText.text = "MONEY: " + money;
        healthBar.value = currentPlayerHealth;

        if (currentPlayerHealth <= 0)
            deathPanel.SetActive(true);
    }

    public void PlaceObject()
    {
        if (money >= objectSelected.money)
        {
            if (selectedPlatform.currentActor)
            {
                Destroy(objectSelected.gameObject);
                objectSelected = null;
                return;
            }

            objectSelected.transform.position = selectedPlatform.transform.position;
            objectSelected.isActive = true;
            objectSelected.myPlatform = selectedPlatform;
            selectedPlatform.currentActor = objectSelected;

            if (!objectSelected.paid)
            {
                money -= objectSelected.money;
                objectSelected.paid = true;
            }

            objectSelected = null;
        }
        else
            Destroy(objectSelected.gameObject);


    }

}
