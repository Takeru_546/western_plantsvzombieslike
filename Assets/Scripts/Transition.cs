﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Transition
{
    public Decision decision;
    public State[] trueState = new State[1];
    public State[] falseState = new State[1];
}
