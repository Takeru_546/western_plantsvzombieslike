﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ObjectCard : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public Actor dragObject;
    public TextMeshProUGUI moneyText;

    public void OnDrag(PointerEventData eventData)
    {
            
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!Player.instance.objectSelected)
            Player.instance.objectSelected = Instantiate(dragObject);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(Player.instance.selectedPlatform)
        {
            if(Player.instance.selectedPlatform.currentActor == null)
                Player.instance.PlaceObject(); 
        }
        else
        Destroy(Player.instance.objectSelected);
    }

    private void Update()
    {
        moneyText.text = dragObject.money.ToString();
    }
}
