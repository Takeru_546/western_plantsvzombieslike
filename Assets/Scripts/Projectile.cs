﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Rigidbody rb;

    public float force;
    public float lifeTime;
    public int damage;

    public Actor.Type targetType;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Invoke("SelfDestruct", lifeTime);
    }

    void Update()
    {
        rb.AddForce(transform.forward * force * Time.deltaTime, ForceMode.Impulse);
    }

    public void SelfDestruct()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        var otherActor = other.GetComponent<Actor>();
        if(otherActor)
        {
            if (otherActor.npcType == targetType)
            {
                otherActor.TakeDamage(damage);
                SelfDestruct();
            }
        }

    }
}
